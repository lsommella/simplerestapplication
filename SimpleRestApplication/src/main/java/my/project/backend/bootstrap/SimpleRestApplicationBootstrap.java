package my.project.backend.bootstrap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;


@SpringBootApplication
@ComponentScan("my.project*")
@PropertySource(value="file:D:\\environment\\workspace\\simplerestapplication\\SimpleRestApplication\\resources\\config\\application.properties")

public class SimpleRestApplicationBootstrap {

	public static void main(String[] args) {

		SpringApplication.run(SimpleRestApplicationBootstrap.class, args);
	}

}
