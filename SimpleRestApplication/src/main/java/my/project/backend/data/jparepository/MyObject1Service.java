package my.project.backend.data.jparepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import my.project.backend.data.entity.MyObjectEnity1;
public abstract class MyObject1Service implements JpaRepository<MyObjectEnity1, Long>{
	public abstract  List<MyObjectEnity1> findAll();
	public abstract Optional<MyObjectEnity1> findById(Long id);
	public abstract MyObjectEnity1 save(MyObjectEnity1 stock);
	public abstract void deleteById(Long id);

}
