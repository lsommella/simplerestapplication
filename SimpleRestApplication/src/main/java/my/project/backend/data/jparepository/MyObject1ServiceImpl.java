package my.project.backend.data.jparepository;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import my.project.backend.data.entity.MyObjectEnity1;

@Service
@RequiredArgsConstructor
@Component
public abstract class MyObject1ServiceImpl extends  MyObject1Service{
	private static MyObject1Service myObject1Service;
    
    public  List<MyObjectEnity1> findAll() {
        return myObject1Service.findAll();
    }
    public Optional<MyObjectEnity1> findById(Long id) {
        return myObject1Service.findById(id);
    }
    public MyObjectEnity1 save(MyObjectEnity1 stock) {
        return myObject1Service.save(stock);
    }
    public void deleteById(Long id) {
    	myObject1Service.deleteById(id);
    }


}
