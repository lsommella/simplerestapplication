package my.project.backend.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import my.project.backend.data.entity.MyObjectEnity1;
import my.project.backend.data.jparepository.MyObject1Service;


@RestController
@RequestMapping(value= "/simpleRest")
@Slf4j

public class SimpleRestApi {
	@Autowired	
	private  MyObject1Service myObject1Service;

	@GetMapping(path="/request")
	public String simpleGetRequest(@RequestParam(value="x") Integer x,@RequestParam(value="y")Integer y ) {		
		
		
		return "Hi";
	}

    @GetMapping
    public ResponseEntity<List<MyObjectEnity1>> findAll() {
        return ResponseEntity.ok(myObject1Service.findAll());
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody MyObjectEnity1 myObject1Service) {
        return ResponseEntity.ok(((CrudRepository<MyObjectEnity1, Long>) myObject1Service).save(myObject1Service));
    }

    @GetMapping("/{id}")
    public ResponseEntity<MyObjectEnity1> findById(@PathVariable Long id) {
        Optional<MyObjectEnity1> stock = myObject1Service.findById(id);
        if (!stock.isPresent()) {
            //log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(stock.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<MyObjectEnity1> update(@PathVariable Long id, @Valid @RequestBody MyObjectEnity1 product) {
        if (!myObject1Service.findById(id).isPresent()) {
          //  log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(myObject1Service.save(product));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!myObject1Service.findById(id).isPresent()) {
         //   log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        myObject1Service.deleteById(id);

        return ResponseEntity.ok().build();
    }
}


